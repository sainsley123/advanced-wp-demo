import { Component, OnInit } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {HttpService} from "../services/http/http.service";


@Component({
    moduleId: module.id,
    selector: 'nav-component',
    templateUrl: 'nav.component.html'
})
export class NavComponent implements OnInit  {
    apiUrl: string = "http://localhost/advanced-wp/wp-json/wp-api-menus/v2/menus/2";
    menuItems: any;

    constructor(private _httpService: HttpService){}

    ngOnInit(){
        this.getMenu();
    }

    getMenu(){
        this._httpService.getData(this.apiUrl)
            .subscribe(data => { this.menuItems = data.items });
    }
}