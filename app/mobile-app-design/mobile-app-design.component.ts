import { Component } from '@angular/core';
import {HttpService} from "../services/http/http.service";

@Component({
    moduleId: module.id,
    selector: 'mobile-app-design',
    templateUrl: 'mobile-app-design.component.html'
})
export class MobileAppDesignComponent {
    apiUrl: string = "http://localhost/advanced-wp/wp-json/wp/v2/pages?slug=mobile-app-design";
    pageContent: Array<string>;

    constructor(private _httpService: HttpService){}

    ngOnInit(){
        this.getPage();
    }

    getPage(){
        this._httpService.getData(this.apiUrl)
            .subscribe(data => { this.pageContent = data });
    }
}