"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var app_component_1 = require('./app.component');
var home_component_1 = require('./home/home.component');
var mobile_app_design_component_1 = require('./mobile-app-design/mobile-app-design.component');
var nav_component_1 = require('./nav/nav.component');
var graphic_design_component_1 = require("./graphic-design/graphic-design.component");
var web_design_component_1 = require("./web-design/web-design.component");
var video_production_component_1 = require("./video-production/video-production.component");
var page_not_found_component_1 = require("./PageNotFound/page-not-found.component");
var page_component_1 = require("./page/page.component");
var routes = [
    { path: '', component: home_component_1.HomeComponent },
    //{path: 'home', redirectTo: '', pathMatch: 'full'},
    //{path: 'mobile-app-design', component: MobileAppDesignComponent},
    //{path: 'graphic-design', component: GraphicDesignComponent},
    //{path: 'web-design', component: WebDesignComponent},
    //{path: 'video-production', component: VideoProductionComponent}
    { path: ':name', component: page_component_1.PageComponent },
    { path: '**', component: page_not_found_component_1.PageNotFoundComponent }
];
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forRoot(routes)],
            exports: [router_1.RouterModule],
        }), 
        __metadata('design:paramtypes', [])
    ], AppRoutingModule);
    return AppRoutingModule;
}());
exports.AppRoutingModule = AppRoutingModule;
exports.routedComponents = [
    app_component_1.AppComponent,
    home_component_1.HomeComponent,
    mobile_app_design_component_1.MobileAppDesignComponent,
    nav_component_1.NavComponent,
    graphic_design_component_1.GraphicDesignComponent,
    web_design_component_1.WebDesignComponent,
    video_production_component_1.VideoProductionComponent,
    page_not_found_component_1.PageNotFoundComponent,
    page_component_1.PageComponent
];
//# sourceMappingURL=app.routing.js.map