import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { AppRoutingModule, routedComponents } from './app.routing';

import { AppComponent } from './app.component';
import { HttpService } from "./services/http/http.service";



@NgModule({
    imports: [
       BrowserModule,
       HttpModule,
       AppRoutingModule
    ],
    declarations: [
       routedComponents
    ],
    providers: [ HttpService ],
    bootstrap: [ AppComponent ]
})
export class AppModule {}