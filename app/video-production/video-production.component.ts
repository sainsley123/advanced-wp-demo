import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'video-production',
    templateUrl: 'video-production.component.html'
})
export class VideoProductionComponent {}