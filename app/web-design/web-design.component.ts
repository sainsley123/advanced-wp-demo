import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'web-design',
    templateUrl: 'web-design.component.html'
})
export class WebDesignComponent {}