import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'graphic-design',
    templateUrl: 'graphic-design.component.html'
})
export class GraphicDesignComponent {}