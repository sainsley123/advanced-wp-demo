import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { MobileAppDesignComponent } from './mobile-app-design/mobile-app-design.component';
import { NavComponent } from './nav/nav.component';
import { GraphicDesignComponent } from "./graphic-design/graphic-design.component";
import { WebDesignComponent } from "./web-design/web-design.component";
import { VideoProductionComponent } from "./video-production/video-production.component";
import {PageNotFoundComponent} from "./PageNotFound/page-not-found.component";
import {PageComponent} from "./page/page.component";

const routes: Routes = <Routes> [
  {path: '', component: HomeComponent},
  //{path: 'home', redirectTo: '', pathMatch: 'full'},
  //{path: 'mobile-app-design', component: MobileAppDesignComponent},
  //{path: 'graphic-design', component: GraphicDesignComponent},
  //{path: 'web-design', component: WebDesignComponent},
  //{path: 'video-production', component: VideoProductionComponent}
  {path: ':name', component: PageComponent},
  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }

export const routedComponents = [
    AppComponent,
    HomeComponent,
    MobileAppDesignComponent,
    NavComponent,
    GraphicDesignComponent,
    WebDesignComponent,
    VideoProductionComponent,
    PageNotFoundComponent
    PageComponent
];