import { Component, OnInit } from '@angular/core';
import {HttpService} from "../services/http/http.service";

@Component({
    moduleId: module.id,
    selector: 'home',
    templateUrl: 'home.component.html'
})
export class HomeComponent implements OnInit {
    apiUrl: string = "http://localhost/advanced-wp/wp-json/wp/v2/pages?slug=home";
    pageContent: Array<string>;

    constructor(private _httpService: HttpService){}

    ngOnInit(){
       this.getPage();
    }

    getPage(){
       this._httpService.getData(this.apiUrl)
           .subscribe(data => { this.pageContent = data });
    }

}