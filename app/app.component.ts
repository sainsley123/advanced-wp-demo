import { Component, OnInit } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'myAwesomeApp',
    templateUrl: 'app.component.html'
})
export class AppComponent {}