import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import 'rxjs/add/operator/switchMap';
import { HttpService } from "../services/http/http.service";
import 'rxjs/add/operator/switchMap';
import {Observable} from "rxjs/Observable";

@Component({
    moduleId: module.id,
    selector: 'page',
    templateUrl: 'page.component.html'
})
export class PageComponent implements OnInit{

    constructor(private _route: ActivatedRoute, private _httpService: HttpService){}

    pageContent: Observable <any>;
    pageSlug: string;
    baseUrl: string = "http://localhost/advanced-wp/wp-json/wp/v2/pages?slug=";
    returnContent: string;

    ngOnInit(){
        this.pageContent = this._route.params
            .switchMap((params: Params) => {
                this.pageSlug = params['name'];
                this.pageContent = this._httpService.getData(this.baseUrl + this.pageSlug);
                return this.pageContent;
            });
        this.pageContent.subscribe(data => this.returnContent = data);
    }
}