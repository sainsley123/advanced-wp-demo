"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
require('rxjs/add/operator/switchMap');
var http_service_1 = require("../services/http/http.service");
require('rxjs/add/operator/switchMap');
var PageComponent = (function () {
    function PageComponent(_route, _httpService) {
        this._route = _route;
        this._httpService = _httpService;
        this.baseUrl = "http://localhost/advanced-wp/wp-json/wp/v2/pages?slug=";
    }
    PageComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.pageContent = this._route.params
            .switchMap(function (params) {
            _this.pageSlug = params['name'];
            _this.pageContent = _this._httpService.getData(_this.baseUrl + _this.pageSlug);
            return _this.pageContent;
        });
        this.pageContent.subscribe(function (data) { return _this.returnContent = data; });
    };
    PageComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'page',
            templateUrl: 'page.component.html'
        }), 
        __metadata('design:paramtypes', [router_1.ActivatedRoute, http_service_1.HttpService])
    ], PageComponent);
    return PageComponent;
}());
exports.PageComponent = PageComponent;
//# sourceMappingURL=page.component.js.map